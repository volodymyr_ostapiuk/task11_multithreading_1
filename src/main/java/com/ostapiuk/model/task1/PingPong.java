package com.ostapiuk.model.task1;

import java.util.Random;

public class PingPong {
    private final Object ball = new Object();
    private boolean isHit = true;

    public void startPlayTennis() {
        Thread ping = new Thread(() -> {
            Thread.currentThread().setName("Ping");
            play();
        });
        Thread pong = new Thread(() -> {
            Thread.currentThread().setName("Pong");
            play();
        });
        ping.start();
        pong.start();
    }

    private void play() {
        while (isHit) {
            synchronized (ball) {
                try {
                    Thread.sleep(250);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                hitTheBall();
            }
        }
    }

    private void hitTheBall() {
        Random random = new Random();
        isHit = random.nextBoolean() || random.nextBoolean();
        if (isHit) {
            System.out.println(Thread.currentThread().getName());
            ball.notify();
            try {
                ball.wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        } else {
            System.exit(0);
        }
    }

    public static void main(String[] args) {
        PingPong pingPong = new PingPong();
        pingPong.startPlayTennis();
    }
}