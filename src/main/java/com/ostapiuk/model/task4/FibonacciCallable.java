package com.ostapiuk.model.task4;

import com.ostapiuk.model.task2.FibonacciNumber;

import java.util.concurrent.Callable;

public class FibonacciCallable implements Callable {
    private final int size;

    public FibonacciCallable(int n) {
        size = n;
    }

    @Override
    public Object call() {
        return new FibonacciNumber(size).getFibonacciNumbers().stream()
                .mapToInt(Integer::intValue).sum();
    }
}
