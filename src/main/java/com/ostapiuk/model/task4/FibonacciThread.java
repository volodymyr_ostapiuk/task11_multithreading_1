package com.ostapiuk.model.task4;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;

public class FibonacciThread {
    private final int size;

    public FibonacciThread(int N) {
        size = N;
    }

    public void createThreads() {
        List<FutureTask> futureTaskList = new ArrayList<>();
        for (int i = 0; i < 3; i++) {
            futureTaskList.add(new FutureTask(new FibonacciCallable(size)));
        }
        for (FutureTask task : futureTaskList) {
            new Thread(task).start();
            try {
                System.out.println("Sum of " + size + " Fibonacci numbers = " + task.get());
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            }
        }
    }

    public static void main(String[] args) {
        FibonacciThread fibonacciThread = new FibonacciThread(5);
        fibonacciThread.createThreads();
    }
}
