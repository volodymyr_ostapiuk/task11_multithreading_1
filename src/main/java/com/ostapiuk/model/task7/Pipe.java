package com.ostapiuk.model.task7;

import java.io.IOException;
import java.io.PipedReader;
import java.io.PipedWriter;
import java.util.Arrays;

public class Pipe {
    private PipedWriter pipedWriter = new PipedWriter();
    private PipedReader pipedReader = new PipedReader();
    private String[] message = {"1", "2", "3", "4", "5"};

    public void startPipe() {
        try {
            pipedWriter.connect(pipedReader);
        } catch (IOException e) {
            e.printStackTrace();
        }
        Thread reader = new Thread(this::readData);
        Thread writer = new Thread(this::writeData);
        reader.start();
        writer.start();
        try {
            reader.join();
            writer.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void readData() {
        int position;
        char[] buffer = new char[50];
        char[] word;
        try {
            position = pipedReader.read(buffer);
            while (position != -1) {
                word = Arrays.copyOf(buffer, position);
                System.out.println("Read: " + new String(word));
                Thread.sleep(1000);
                position = pipedReader.read(buffer);
            }
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        } finally {
            try {
                pipedReader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void writeData() {
        try {
            for (String word : message) {
                pipedWriter.write(word);
                System.out.println("Write: " + word);
                Thread.sleep(1000);
            }
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        } finally {
            try {
                pipedWriter.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static void main(String[] args) {
        Pipe pipe = new Pipe();
        pipe.startPipe();
    }
}
