package com.ostapiuk.model.task2;

public class Sequence {
    private FibonacciNumber fibonacciNumber;

    public Sequence(int n) {
        fibonacciNumber = new FibonacciNumber(n);
    }

    public void startTask() {
        int numberOfTasks = 5;
        for (int i = 1; i <= numberOfTasks; i++) {
            int paused = 500;
            Thread thread = new Thread(() -> {
                fibonacciNumber.showFibonacciNumbers(paused);
            });
            thread.start();
        }
    }

    public static void main(String[] args) {
        Sequence sequence = new Sequence(5);
        sequence.startTask();
    }
}
