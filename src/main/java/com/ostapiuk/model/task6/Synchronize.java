package com.ostapiuk.model.task6;

public class Synchronize {
    public void startSameObjectSync() throws InterruptedException {
        System.out.println("Synchronize on the same object");
        SameObjectSynchronize sameObjectSync = new SameObjectSynchronize();
        startAndJoinThread(new Thread(sameObjectSync::method1),
                new Thread(sameObjectSync::method2),
                new Thread(sameObjectSync::method3));
    }

    public void startDiffObjectSync() throws InterruptedException {
        System.out.println("Synchronize on the different objects");
        DifferentObjectSynchronize diffObjectSync = new DifferentObjectSynchronize();
        startAndJoinThread(new Thread(diffObjectSync::method1),
                new Thread(diffObjectSync::method2),
                new Thread(diffObjectSync::method3));
    }

    private void startAndJoinThread(Thread thread1, Thread thread2, Thread thread3)
            throws InterruptedException {
        thread1.start();
        thread2.start();
        thread3.start();
        thread1.join();
        thread2.join();
        thread3.join();
    }

    public static void main(String[] args) throws InterruptedException {
        Synchronize synchronize = new Synchronize();
        synchronize.startSameObjectSync();
        synchronize.startDiffObjectSync();
    }
}
