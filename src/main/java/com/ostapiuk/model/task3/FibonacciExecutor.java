package com.ostapiuk.model.task3;

import com.ostapiuk.model.task2.FibonacciNumber;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class FibonacciExecutor {
    private FibonacciNumber fibonacciNumber;

    public FibonacciExecutor(int n) {
        fibonacciNumber = new FibonacciNumber(n);
    }

    public void startExecutors() {
        startSingleThreadExecutor();
        startFixedThreadPool();
        startCachedThreadPool();
    }

    private void startSingleThreadExecutor() {
        ExecutorService singleThreadExecutor = Executors.newSingleThreadExecutor();
        singleThreadExecutor.execute(() -> {
            System.out.println(Thread.currentThread().getName());
            fibonacciNumber.showFibonacciNumbers(1000);
        });
        singleThreadExecutor.shutdown();
    }

    private void startFixedThreadPool() {
        ExecutorService fixedThreadPool = Executors.newFixedThreadPool(3);
        fixedThreadPool.submit(() -> {
            System.out.println(Thread.currentThread().getName());
            fibonacciNumber.showFibonacciNumbers(1000);
        });
        fixedThreadPool.shutdown();
    }

    private void startCachedThreadPool() {
        ExecutorService cachedThreadPool = Executors.newCachedThreadPool();
        cachedThreadPool.submit(() -> {
            System.out.println(Thread.currentThread().getName());
            fibonacciNumber.showFibonacciNumbers(1000);
        });
        cachedThreadPool.shutdown();
    }

    public static void main(String[] args) {
        FibonacciExecutor fibonacciExecutors = new FibonacciExecutor(5);
        fibonacciExecutors.startExecutors();
    }
}
